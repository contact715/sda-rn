#import "RCTReactCustomMethod.h"

@implementation RCTReactCustomMethod

RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD(getPhoneID:(RCTPromiseResolveBlock) resolve :(RCTPromiseRejectBlock) reject) {
  NSString *deviceName = [[UIDevice currentDevice] name];
  resolve(deviceName);
}

@end
