import React, {useEffect} from 'react';
import {SafeAreaProvider, SafeAreaView} from 'react-native-safe-area-context';
import {NavigationContainer} from '@react-navigation/native';
import EStyleSheet from 'react-native-extended-stylesheet';
import crashlytics from '@react-native-firebase/crashlytics';

import {ApiProvider} from './src/providers/ApiProvider';

import {RootStack} from './src/navigators';

EStyleSheet.build({
  $background: '#15171B',
  $primary: '#4858F5',
});

function App() {
  useEffect(() => {
    crashlytics().log('App mounted');
  }, []);

  return (
    <NavigationContainer>
      <ApiProvider>
        <SafeAreaProvider>
          <SafeAreaView
            edges={['bottom', 'left', 'right', 'top']}
            style={styles.safeAreaView}>
            <RootStack />
          </SafeAreaView>
        </SafeAreaProvider>
      </ApiProvider>
    </NavigationContainer>
  );
}

export default App;

const styles = EStyleSheet.create({
  safeAreaView: {
    flex: 1,
    backgroundColor: '$background',
  },
});
