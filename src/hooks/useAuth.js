import {create} from 'zustand';

export const useAuth = create(set => ({
  user: null,
  signIn: email => set({user: {email}}),
  signOut: () => set({user: null}),
}));
