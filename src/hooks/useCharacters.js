import {useQuery} from '@tanstack/react-query';

import {request} from '../api/request';

const URL = 'https://rickandmortyapi.com/api/character';

export function useCharactersGet() {
  return useQuery({
    queryKey: ['characters'],
    queryFn: () => request(URL),
    placeholderData: {
      results: [],
    },
  });
}
