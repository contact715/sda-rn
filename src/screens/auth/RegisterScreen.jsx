import React, {useEffect} from 'react';
import {View, Text, Button, PermissionsAndroid} from 'react-native';
import messaging from '@react-native-firebase/messaging';

async function requestUserPermission() {
  try {
    const authStatus = await messaging().requestPermission();

    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;

    if (enabled) {
      console.log('Authorization status:', authStatus);
    }
  } catch (e) {
    console.log(e);
  }
}

export const RegisterScreen = ({navigation}) => {
  const {push, navigate, canGoBack, goBack, pop, replace} = navigation;

  // useEffect(() => {
  //   console.log('render');
  //   // messaging().setAPNSToken()
  //   messaging().getAPNSToken().then(console.log);
  // }, []);

  return (
    <View>
      <Text>RegisterScreen</Text>
      <Button
        title="Go to Login"
        onPress={() =>
          navigate('Login', {
            email: 'xx@xx.xx',
          })
        }
      />
      <Button
        title="Request"
        onPress={async () => {
          await requestUserPermission();
          await messaging().getToken().then(console.log);
        }}
      />
    </View>
  );
};
