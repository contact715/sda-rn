import React, {useEffect} from 'react';
import {View, Text, StatusBar} from 'react-native';
import {useForm} from 'react-hook-form';
import EStyleSheet from 'react-native-extended-stylesheet';
import {TextButton} from '../../components/TextButton';
import crashlytics from '@react-native-firebase/crashlytics';

import {getItem, setItem} from '../../utils/storage';
import {PrimaryButton} from '../../components/PrimaryButton';
import {ControlledInput} from '../../components/ControlledInput';
import {useAuth} from '../../hooks/useAuth';

const defaultValues = {
  email: '',
  password: '',
};

export const LoginScreen = ({navigation}) => {
  const signIn = useAuth(state => state.signIn);

  const {
    control,
    handleSubmit,
    setValue,
    formState: {isValid, isSubmitting},
  } = useForm({
    defaultValues,
    mode: 'onBlur',
    // resolver
  });

  useEffect(() => {
    const getUserEmail = async () => {
      const email = await getItem('@nazwa-aplikacji/user-email');
      setValue('email', email);
    };
    getUserEmail();
  }, [setValue]);

  const onSubmit = async data => {
    crashlytics().log('User signed in');
    await Promise.all([
      crashlytics().setUserId(data.email),
      crashlytics().setAttribute('credits', '0'),
    ]);
    setItem('@nazwa-aplikacji/user-email', data.email);
    signIn(data.email);
    // navigation.navigate('Dashboard');
  };

  return (
    <View style={styles.container}>
      <StatusBar barStyle="light-content" backgroundColor="#15171B" />
      <View>
        <Text style={styles.header}>Welcome back!</Text>
        <Text style={styles.subHeader}>Please sign in to your account</Text>
      </View>
      <View style={styles.form}>
        <ControlledInput
          rules={{
            required: true,
          }}
          name="email"
          control={control}
        />
        <ControlledInput
          rules={{
            required: true,
          }}
          name="password"
          control={control}
          secureTextEntry
        />
      </View>
      <View style={styles.buttonContainer}>
        <PrimaryButton
          disabled={!isValid || isSubmitting}
          onPress={handleSubmit(onSubmit)}
          label="Sign In"
        />
        <TextButton
          onPress={() => {
            navigation.navigate('Register');
          }}>
          <Text style={styles.white}>
            Don't Have an Account? <Text style={styles.signUp}>Sign Up</Text>
          </Text>
        </TextButton>
      </View>
    </View>
  );
};

const styles = EStyleSheet.create({
  header: {
    color: '#fff',
    fontSize: 24,
    fontWeight: '600',
    paddingBottom: 8,
  },
  subHeader: {
    color: '#bbb',
    marginBottom: 24,
  },
  container: {
    alignItems: 'center',
    flex: 1,
    backgroundColor: '$background',
    paddingHorizontal: 16,
    justifyContent: 'space-around',
  },
  textInput: {
    borderRadius: 12,
    backgroundColor: '#21242A',
    width: '100%',
    padding: 24,
    color: '#fff',
  },
  form: {
    gap: 8,
    width: '100%',
  },
  buttonContainer: {
    width: '100%',
    gap: 16,
  },
  signUp: {
    color: '#4858F5',
  },
  white: {
    color: '#fff',
  },
});
