import React from 'react';
import {View, Text} from 'react-native';

import {CharactersView} from './CharactersView';
import {CharactersData} from './CharactersData';

export function CharactersScreen({navigation}) {
  return (
    <CharactersData>
      {state => {
        if (state.isLoading) {
          return (
            <View>
              <Text>IsLoading...</Text>
            </View>
          );
        }

        if (state.isError) {
          return (
            <View>
              <Text>isError...</Text>
            </View>
          );
        }

        // wariant 1
        // return <CharactersView data={state.data} />;

        // wariant 2
        return (
          <CharactersView
            data={state.data}
            refetch={state.refetch}
            isRefetching={state.isRefetching}
          />
        );
      }}
    </CharactersData>
  );
}
