import {useCharactersGet} from '../../hooks';

export function CharactersData({children: renderCallback}) {
  const {data, status, refetch, isRefetching} = useCharactersGet();

  if (status === 'pending') {
    return renderCallback({
      isLoading: true,
    });
  }

  if (status === 'error') {
    return renderCallback({
      isError: true,
      isLoading: false,
      isRefetching,
      refetch,
    });
  }

  return renderCallback({
    data,
    isLoading: false,
    isError: false,
    refetch,
    isRefetching,
  });
}
