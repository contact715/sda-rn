import React from 'react';
import {View, RefreshControl} from 'react-native';
import {MasonryFlashList} from '@shopify/flash-list';

import {Character} from '../../components/Character';

export function CharactersView({data, isRefetching, refetch}) {
  return (
    <View style={{marginTop: 50, flex: 1}}>
      <MasonryFlashList
        estimatedItemSize={100}
        numColumns={2}
        keyExtractor={item => item.id.toString()}
        data={data?.results ?? []}
        renderItem={({item}) => <Character {...item} />}
        refreshControl={
          <RefreshControl refreshing={isRefetching} onRefresh={refetch} />
        }
      />
    </View>
  );
}
