import React from 'react';
import WebView from 'react-native-webview';

export const WebViewScreen = () => {
  return (
    <WebView
      onLoad={() => console.log('onLoad')}
      onError={() => console.log('onError')}
      onLoadStart={() => console.log('onLoadStart')}
      onLoadEnd={() => console.log('onLoadEnd')}
      source={{
        uri: 'https://www.google.com',
        // html: `<p>Custom HTML</p>`,
      }}
    />
  );
};
