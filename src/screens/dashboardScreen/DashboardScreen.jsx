import React from 'react';
import {Button, Text, View, NativeModules} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import crashlytics from '@react-native-firebase/crashlytics';

import {useAuth} from '../../hooks/useAuth';

const styles = EStyleSheet.create({
  container: {
    flex: 1,
  },
});

export function DashboardScreen({navigation}) {
  const {ReactCustomMethod} = NativeModules;

  const {signOut, user} = useAuth(state => ({
    user: state.user,
    signOut: state.signOut,
  }));

  const getId = async () => {
    try {
      const id = await ReactCustomMethod.getPhoneID();
      console.log(id);
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <View style={styles.container}>
      <Text>Hello {user?.email ?? 'N/A'}</Text>
      <Button onPress={getId} title="Get phone id" />
      <Button
        title="Logout"
        onPress={() => {
          crashlytics().crash();
          // signOut();
          // navigation.replace('Login');
        }}
      />
    </View>
  );
}
