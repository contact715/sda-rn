import React from 'react';
import {Controller} from 'react-hook-form';

import {Input} from './Input';

export const ControlledInput = ({name, control, rules, ...rest}) => {
  return (
    <Controller
      rules={rules}
      name={name}
      control={control}
      render={({
        field: {onChange, name: fieldName, onBlur, value, disabled},
      }) => (
        <Input
          name={fieldName}
          onBlur={onBlur}
          value={value}
          disabled={disabled}
          onChangeText={onChange}
          {...rest}
        />
      )}
    />
  );
};
