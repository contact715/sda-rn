import React from 'react';
import {Text, Image, Pressable, Linking} from 'react-native';

export function Character({name, image, status, url}) {
  const handleClick = async () => {
    const canOpen = await Linking.canOpenURL(url);

    if (!canOpen) {
      //  TODO log error

      return;
    }
    await Linking.openURL(url);
  };

  return (
    <Pressable
      onPress={handleClick}
      style={{
        margin: 4,
        borderRadius: 8,
        backgroundColor: 'lightgrey',
        overflow: 'hidden',
      }}>
      <Image
        source={{
          uri: image,
        }}
        style={{
          // borderTopLeftRadius: 8,
          width: 100,
          height: 100,
          // borderRadius: 50,
          // marginBottom: 4,
        }}
      />
      <Text>
        {name} - {status}
      </Text>
    </Pressable>
  );
}
