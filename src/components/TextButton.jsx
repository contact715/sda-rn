import React from 'react';
import EStyleSheet from 'react-native-extended-stylesheet';
import {Text, Pressable} from 'react-native';

export const TextButton = ({onPress, label, children}) => {
  return (
    <Pressable
      onPress={onPress}
      style={({pressed}) => [styles.pressable, pressed ? styles.opacity : {}]}>
      {children ?? <Text style={styles.text}>{label}</Text>}
    </Pressable>
  );
};

const styles = EStyleSheet.create({
  opacity: {
    opacity: 0.5,
  },
  pressable: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    color: '#fff',
  },
});
