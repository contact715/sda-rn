import React from 'react';
import EStyleSheet from 'react-native-extended-stylesheet';
import {Text, Pressable} from 'react-native';

export const PrimaryButton = ({label, onPress, ...rest}) => {
  return (
    <Pressable
      onPress={onPress}
      style={({pressed}) => [styles.container, pressed ? styles.opacity : {}]}
      {...rest}>
      <Text style={styles.text}>{label}</Text>
    </Pressable>
  );
};

const styles = EStyleSheet.create({
  container: {
    padding: 24,
    backgroundColor: '$primary',
    borderRadius: 24,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  opacity: {
    opacity: 0.5,
  },
  text: {
    color: '#fff',
    fontWeight: '600',
  },
});
