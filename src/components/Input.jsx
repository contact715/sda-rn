import React from 'react';
import {TextInput} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';

export const Input = props => {
  return <TextInput style={styles.input} {...props} />;
};
const styles = EStyleSheet.create({
  input: {
    borderRadius: 12,
    backgroundColor: '#21242A',
    width: '100%',
    padding: 24,
    color: '#fff',
  },
});
