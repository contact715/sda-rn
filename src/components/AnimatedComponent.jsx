import React from 'react';
import {Button, View} from 'react-native';
import Animated, {
  useAnimatedStyle,
  useSharedValue,
  withTiming,
} from 'react-native-reanimated';

export const AnimatedComponent = () => {
  const size = useSharedValue(100);
  const animatedStyles = useAnimatedStyle(() => ({
    width: size.value,
    height: size.value,
    backgroundColor: 'blue',
  }));

  const handlePress = () => {
    size.value = withTiming(size.value === 100 ? 200 : 100);
  };

  return (
    <View>
      <Animated.View style={animatedStyles} />
      <Button title="Press me" onPress={handlePress} />
    </View>
  );
};
