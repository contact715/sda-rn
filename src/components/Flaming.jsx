import React from 'react';
import {View} from 'react-native';
import LottieView from 'lottie-react-native';
import EStyleSheet from 'react-native-extended-stylesheet';

import animationJson from './flaming.json';

export const Flaming = () => {
  return (
    <View style={styles.container}>
      <LottieView
        source={animationJson}
        style={styles.lottieView}
        autoPlay
        loop
      />
    </View>
  );
};

const styles = EStyleSheet.create({
  container: {
    flex: 1,
  },
  lottieView: {
    flex: 1,
  },
});
