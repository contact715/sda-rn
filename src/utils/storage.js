import AsyncStorage from '@react-native-async-storage/async-storage';

export const setItem = async (key, payload) => {
  try {
    await AsyncStorage.setItem(key, payload);
  } catch (e) {
    console.log('Error setting item in storage', e);
  }
};

export const getItem = async key => {
  try {
    const value = await AsyncStorage.getItem(key);
    return value;
  } catch (e) {
    console.log('Error getting item from storage', e);
  }
};
