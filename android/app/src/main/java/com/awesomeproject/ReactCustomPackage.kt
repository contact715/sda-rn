package com.awesomeproject

import com.facebook.react.ReactPackage
import com.facebook.react.bridge.NativeModule
import com.facebook.react.bridge.ReactApplicationContext
import com.facebook.react.uimanager.ViewManager

class ReactCustomPackage : ReactPackage {
    override fun createNativeModules(reactContext: ReactApplicationContext): MutableList<NativeModule> {
        val modules = mutableListOf<NativeModule>()
        modules.add(ReactCustomMethod(reactContext))
        return modules
    }

    override fun createViewManagers(createContext: ReactApplicationContext): List<ViewManager<*, *>> {
        return emptyList()
    }
}