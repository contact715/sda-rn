package com.awesomeproject

import android.annotation.SuppressLint
import android.provider.Settings
import com.facebook.react.bridge.Promise
import com.facebook.react.bridge.ReactApplicationContext
import com.facebook.react.bridge.ReactContextBaseJavaModule
import com.facebook.react.bridge.ReactMethod
import java.lang.Exception

class ReactCustomMethod(private val reactContext: ReactApplicationContext) : ReactContextBaseJavaModule(reactContext) {
    override fun getName(): String {
        return "ReactCustomMethod"
    }

    @ReactMethod
    fun getPhoneID(response: Promise) {
        try {
            @SuppressLint("HardwareIds")
            val id = Settings.Secure.getString(reactContext.contentResolver, Settings.Secure.ANDROID_ID)
            response.resolve(id)

        } catch (e: Exception) {
            response.reject("Error", e)
        }
    }
}